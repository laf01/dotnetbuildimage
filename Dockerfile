# escape=`

FROM mcr.microsoft.com/windows/servercore:ltsc2019

# Restore the default Windows shell for correct batch processing.
SHELL ["cmd", "/S", "/C"]
# Look at https://stackoverflow.com/a/65501218
RUN                                                                                            `
    curl -SL --output vs_buildtools.exe https://aka.ms/vs/16/release/vs_buildtools.exe &&      `
                                                                                               `
    (start /w vs_buildtools.exe --quiet --wait --norestart --nocache                           `
                                --add Microsoft.VisualStudio.Workload.ManagedDesktopBuildTools `
                                --add Microsoft.VisualStudio.Component.TextTemplating          `
                                --add Microsoft.VisualStudio.Workload.DataBuildTools           `
                                --add Microsoft.VisualStudio.Component.SQL.SSDTBuildSku        `
     || IF "%ERRORLEVEL%"=="3010" EXIT 0) &&                                                   `
    del /q vs_buildtools.exe

# Define the entry point for the docker container.
# This entry point starts the developer command prompt and launches the PowerShell shell.
ENTRYPOINT ["C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\BuildTools\\Common7\\Tools\\VsDevCmd.bat", "&&", "powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]